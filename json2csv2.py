import json, sys
import pandas as pd

json_src_file = sys.argv[1]
dst_file = sys.argv[2]

with open(json_src_file+".json") as f:
    results = json.load(f)

res_keys = list(results.keys())
new_keys = ["startClient","startComputing","endComputing","imageSize","responseCN"]
old_keys = ["SentAt","ProcessedAt","EndedAt","Size","ReceivedAt"]

dataArray = []

for key in res_keys:
    dataArray.append([results[key][k] for k in old_keys])

data = pd.DataFrame(data=dataArray, columns=new_keys)
data = data.sort_values(by="startComputing").reset_index(drop=True)
data.to_csv("validation/results/sim_"+dst_file+".csv", index=False)