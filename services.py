import json, joblib
import pandas as pd

mapped = {
    "hd": json.load(open("mapped/mapped1.json")),
    "hd1": json.load(open("mapped/mapped.json"))
}

RID = pd.read_csv("randomImageDist.csv")
images_d = {}
for i in range(len(RID)):
    images_d[str(i)] = int(RID["imageSize"][i])
images_dist={
    "hd1": images_d
}

images_d = json.load(open("realExp/images_dists/images_dist5.json"))
images_dist["hd"]=images_d

# processing time function for the hd1 service
def hd1(order, issue):
    model = joblib.load("models/hd.joblib")
    val = mapped["hd1"][str(model.predict([[issue, images_dist["hd1"][order]]])[0])]
    return max(50, val-5), images_dist["hd1"][order]

# processing time function for the hd service
def hd(order, issue):
    model = joblib.load("models/hd1.joblib")
    val = mapped["hd"][str(model.predict([[issue, images_dist["hd"][order]]])[0])]
    return max(55, val), images_dist["hd"][order]
