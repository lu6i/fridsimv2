import pandas as pd
import matplotlib.pyplot as plt
import statistics, joblib
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
import json, sys, os
from kneed import KneeLocator
import numpy as np

project = "test"
nb_req = sys.argv[1]
nb_exp = int(sys.argv[2])

completion_time_mae=""
completion_time_std=""
image_size_std=""
makespan_ae=""

fig = plt.figure()
cdf = fig.add_subplot(111)
tolerances=""
accuracies=""
for n in range(1, nb_exp+1):
    print("***************Number of nodes = "+str(n)+"**********************************************************************")
    #Data completion
    datasets = []
    for j in range(n,n+1):
        data=pd.read_csv("realExp/results/reachy_lille_"+nb_req+"_"+str(j)+".csv")
        data["ind"]=[i for i in range(len(data))]
        data["index"]=[data["startComputing"][i]-min(data["startClient"]) for i in range(len(data))]
        data["etl_duration"]=[data["responseCN"][i]-data["startClient"][i] for i in range(len(data))]
        data["completion"]=[data["endComputing"][i]-data["startComputing"][i] for i in range(len(data))]
        data["makespan"]=[data["endComputing"][i]-min(data["startClient"]) for i in range(len(data))]
        datasets.append(data)
    remote_data = pd.concat(datasets, ignore_index=True)

    images_dist = {}
    for i in range(len(remote_data)):
        images_dist[str(i)] = int(remote_data["imageSize"][i])
    print(type(images_dist))
    json.dump(images_dist, open("realExp/images_dists/images_dist"+str(n)+".json", "w+"))

    #Data features' restriction
    x = remote_data[["index", "imageSize"]].to_numpy()
    y = remote_data["completion"].to_numpy()

    #Data splitting and concatenation
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=40)
    train_df = pd.DataFrame(data=[[x_train[k][0], x_train[k][1], y_train[k]] for k in range(len(x_train))], columns=["index", "imageSize", "completion"])
    test_df = pd.DataFrame(data=[[x_test[k][0], x_test[k][1], y_test[k]] for k in range(len(x_test))], columns=["index", "imageSize", "completion"])
    conseq_data = pd.concat([train_df, test_df], ignore_index=True)

    #Analyzing mean absolute error according to the number of clusters
    accs = []
    for n_clusters in range(2, 280):
        model = KMeans(n_clusters=n_clusters, random_state=1).fit(x_train)
        labels = model.predict(conseq_data[["index", "imageSize"]].to_numpy())
        conseq_data["label"] = [label for label in labels]
        datag = conseq_data.groupby("label")
        conseq_data["prediction"] = [statistics.median(datag.get_group(label)["completion"]) for label in labels]
        acc = statistics.mean([abs(conseq_data["prediction"][p]-conseq_data["completion"][p]) for p in range(280, len(conseq_data))])
        accs.append(acc)

    """plt.plot(range(2, 70), accs)
    plt.xlabel("number of cluster")
    plt.ylabel("Mean absolute error (s)")
    plt.savefig("acc_"+str(n)+".png")
    plt.show()"""

    #Building the model
    n_clusters = accs.index(min(accs))+2
    print("the optimal number of clusters: "+str(n_clusters))

    model = KMeans(n_clusters=n_clusters, random_state=10).fit(x_train)
    labels = model.predict(conseq_data[["index", "imageSize"]].to_numpy())
    conseq_data["label"] = [label for label in labels]
    datag = conseq_data.groupby("label")
    conseq_data["prediction"] = [statistics.median(datag.get_group(label)["completion"]) for label in labels]
    test_df = conseq_data.iloc[280:,:]
    test_df["accuracy"]=[abs(test_df["prediction"][p]-test_df["completion"][p]) for p in range(280,400)]

    #Analyzing the CDF according to the mean absolute error
    nb=10
    r = (max(test_df["accuracy"])-min(test_df["accuracy"]))/nb
    min_ = min(test_df["accuracy"])
    cum = [len(test_df[test_df["accuracy"]<=min_+k*r])/len(test_df) for k in range(nb+1)]
    accs = [(min_+k*r) for k in range(nb+1)]
    cdf.plot(accs, cum, label=str(n)+" node(s)")
    acc = KneeLocator(accs, cum, curve="concave", direction="increasing").knee
    tolerances+=" & "+str(round(acc, 2))
    accuracies+=" & "+str(round(cum[accs.index(acc)]*100, 2))

    #Building and saving the machine learning model
    model = KMeans(n_clusters=10, random_state=10).fit(x)
    labels = model.labels_
    remote_data["label"] = [label for label in labels]
    datag = remote_data.groupby("label")
    remote_data["prediction"] = [statistics.mean(datag.get_group(label)["completion"]) for label in labels]
    joblib.dump(model, "models/hd"+str(n)+".joblib")

    #Saving mapping between clusters and predictions
    mapped = {}
    for label in list(set(labels)):
        mapped[str(label)] = min(datag.get_group(label)["completion"])
    mapped_file = open("mapped/mapped"+str(n)+".json", "w+")
    json.dump(mapped, mapped_file)
    mapped_file.close()

    if os.path.exists("validation/results/sim_reachy_lille_"+nb_req+"_"+str(n)+".csv"):
        sim_data=pd.read_csv("validation/results/sim_reachy_lille_"+nb_req+"_"+str(n)+".csv")

        sim_data["etl_duration"] = [sim_data["responseCN"][i]-sim_data["startClient"][i] for i in range(len(remote_data))]
        sim_data["computing"] = [sim_data["endComputing"][i]-sim_data["startComputing"][i] for i in range(len(remote_data))]
        sim_data["makespan"] = [sim_data["endComputing"][i]-min(sim_data["startClient"]) for i in range(len(remote_data))]
        completion_var = [abs(sim_data["computing"][i]-sim_data["computing"][i+1]) for i in range(len(sim_data)-1)]
        index_var= [sim_data["startClient"][i] for i in range(1, len(sim_data))]
        #Printing mean absolute errors for etl duration and completion time
        etl_mae = statistics.mean([abs(remote_data["etl_duration"][i]-sim_data["etl_duration"][i]) for i in range(len(remote_data))])
        completion_mae = statistics.mean([abs(remote_data["completion"][i]-sim_data["computing"][i]) for i in range(len(remote_data))])
        print("ETL mae: "+str(etl_mae))
        completion_time_mae+=" & "+str(round(completion_mae, 2))
            
        #Printing frequency
        s = 0
        for i in range(len(remote_data)-1):
            s+= (remote_data["startClient"][i+1]-remote_data["startClient"][i])
        print("frequency: "+str((len(remote_data)-1)/s))

        #Printing the request completion time's standard deviation
        completion_time_std+=" & "+str(round(np.std(remote_data['completion']), 2))

        #Printing the image size standard deviation
        image_size_std+=" & "+str(round(np.std(remote_data['imageSize'])/1024, 2))
        
        #Printing the makespan's absolute error
        makespan_ae+=" & "+str(round(abs(max(remote_data["makespan"])-max(sim_data["makespan"])), 2))
    
    
print(completion_time_mae)
print(completion_time_std)
print(image_size_std)
print(makespan_ae)
    
cdf.set_xlabel("Mean absolute error (s)")
cdf.set_ylabel("CDF (%)")
cdf.legend()
fig.savefig("validation/plots/cdf.png")
fig.show()

print("tolerances: "+tolerances)
print("accuracies: "+accuracies)
    