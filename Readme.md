# Simulations in Fog/Cloud Robotics environments

This repository presents a simulator for the deployment of Fog/Cloud Robotics infrastructures, its validation and the estimation of Fog/Cloud resource requirements for Robotics applications.

The presentation of this simulator is given in the file reports/fridsim.pdf and must be read before the rest of the Readme.md.

## Validation of the simulator
This part concerns the experiments carried out to validate the scalability of the simulator and its accuracy.

### Experimental protocol
The use case we study consists in deploying a human face detection service on an image stream. The article submited to CLOSER (in its Section 2) describes in detail this use case. More technically, we have implemented a FAST API (python) to facilitate the realization of the use case, providing client code running on Reachy and server code (main.py) running on grid'5000 nodes (those of the chetemi cluster in Lille). Aware that you will not have a copy of Reachy at your disposal, we have also set up a client program (realExp/codes/iterClient.py) that emulates Reachy (since we are only using it here in a still setting) using the camera of the PC on which it will be run.

1. Upload the files realExp/codes/[all.sh, requirements.txt, setup_and_activate_ipv6.sh, main.py] on your grid'5000 site frontend.
2. Reserve 5 chetemi nodes for a total of 5 hours:
```sh
oarsub -p chetemi -l host=5,walltime=05:00 -I -t deploy
```
3. Deploy a linux operating system on these nodes:
```sh
kadeploy3 debian11-base
```
4. Install on each node the files and dependencies needed to start the service and deploy them in IPV6 so that the VM can be addressed from anywhere via the all.sh file:
```sh
./all.sh site_name(e.g: lille) node1 ... node[numberOfNodes]
```
5. Create screens for each node and start the service on the node in the associated screen:
```sh
screen -S [node_name]
ssh root@[node_name]
uvicorn main:app --host [node_name]-ipv6-lille.grid5000.com --port [port_value]
Ctrl+A
ctrl+D
```
The last two commands allow you to exit the screen without terminating it to create another one.
6. Once the service is deployed on each node, run the client from a PC that has a webcam or a built-in camera:
```sh
python3 realExp/codes/iterClient.py node1-ipv6-lille.grid5000.fr ... node[numberOfNodes]-ipv6-lille.grid5000.fr [portValue] [numberOfRequests] > realExp/results/reachy_lille_[numberOfRequests]_[numberOfNodes].json
```
7. Transform the json file into a csv file: 
```sh
python3 all2csv.py 5 [numberOfRequests]
```

### Machine Learning model evaluation
The build_model.py file allows to generate machine learning models (models/[hd1.joblib, hd2.joblib, hd3.joblib, hd4.joblib, hd5.joblib]) from the traces collected from the execution of the service (via the previous protocol). For each number of chetemi nodes (between 1 and 5) according to the method described in in an article submited to CLOSER (Section 5.1.1.). A second execution of this file also allows to retrieve the data distribution of each experiment to be reproduced in simulation. The service is requested 400 times per number of nodes, which provides us with a database of about 2000 requests. The specificities of the servers not being diversified and the system being homogeneous at each experimentation, we produced a model by number of servers. The goal for each model is to validate it on the other data sets.
To validate the simulator we only used models/hd1.joblib (no matter the number of resources) to demonstrate the strength of the model building approach. For serfri, we will only use models/hd.joblib.
```sh
python3 build_model.py [numberOfRequests] [numberOfExp]
```

### Comparison with the simulation
1. Extract the data distribution used in real life to reproduce them in simulation: 
In this project they are stored at name: realExp/images_dists/images_dist[numberOfNodes].json.
2. Modify the projects/test/robots_info.json file so that it has the usecase configuration.
3. Modify the projects/test/fcns_info.json file so that it has the configuration of the chetemi nodes.
4. Run the simulation with a particular number of nodes (at the line 17 put in the json file corresponding to the correct image distribution):
```sh
python3 project_traces.py test
```
5. Transform the projects/test/simulation_results/accuracy_traces.json file into a csv:
```sh
python3 json2csv2.py projects/test/simulation_results/accuracy_traces reachy_lille_[numberOfRequests]_[numberOfNodes]
```
6. This will generate the file validation/results/sim_reachy_lille_[numberOfRequests]_[numberOfNodes].csv which will be of the same form as realExp/results/reachy_lille_[numberOfRequests]_[numberOfNodes].csv. So we can make comparisons from these two files for each number of nodes between 1 and 5.
```sh
python3 comparing.py [numberOfRequests] [numberOfNodes]
```

## Estimation of Fog/Cloud resource requirements for Robotic applications

The implemented experimentation will be described soon in an article submited to CLOSER (in its section 5.2.) all the tested configurations are in the projects folder (except the test folder).
By executing the file serfri.py:
1. Two matrices are created: the makespans and the financial costs of using the resources, they are respectively represented by the files serfri/results/makespans.csv and serfri/results/costs.csv;
2. We apply the pareto optimization on these two matrices, the solution appears in the file serfri/results/pareto.txt;
3. By normalizing the matrices we obtain the matrices represented respectively by the files serfri/results/normalized_makespans.csv and serfri/results/normalized_costs.csv
4. The maximization of these two normalized matrices gives the matrix serfri/results/MMR.csv
5. The solution expected from serfri, representing the min of the previous matrix appears in the file serfri/results/solution.txt