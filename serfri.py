import json
import pandas as pd

# Cost and Makespan matrices building
makespan_data = []
cost_data = []
columns=["NumberOfResources", "r2_15", "r2_30", "r2_60", "r2_120", "r2_240"]
for config in range(1, 7):
    makespan_data_config = [2**config]
    cost_data_config = [2**config]
    for project in columns[1:]:
        global_results = json.load(open("projects/"+project+"_"+str(config)+"/simulation_results/global_traces.json"))
        keys = list(global_results.keys())[:-1]
        startings = [global_results[key]["Starting"] for key in keys]
        endings = [global_results[key]["Ending"] for key in keys]
        costs = [global_results[key]["CostPerHour"] for key in keys]
        makespan= round(max(endings)-min(startings), 2)
        cost = round(sum([(ending-starting)*(costph)/3600 for starting, ending, costph in zip(startings, endings, costs)]), 2)
        makespan_data_config.append(makespan)
        cost_data_config.append(cost)
    makespan_data.append(makespan_data_config)
    cost_data.append(cost_data_config)

data_makespan = pd.DataFrame(data=makespan_data, columns=columns)
data_makespan.to_csv("serfri/results/makespans.csv", index=False)

data_cost = pd.DataFrame(data=cost_data, columns=columns)
data_cost.to_csv("serfri/results/costs.csv", index=False)

# Pareto front 
def isPareto(x, y, makespans, costs, columns):
    for j in range(len(columns[1:])):
        for i in range(len(makespans)):
            if makespans[columns[1:][j]][i]<=makespans[y][x] and costs[columns[1:][j]][i]<costs[y][x]:
                return False
    return True
text = ""
for resource in columns[1:]:
    for i in range(len(data_cost)):
        test = isPareto(i, resource, data_makespan, data_cost, columns)
        if test:
            text += str(data_makespan["NumberOfResources"][i])+" "+resource+" is a pareto optimum and proposes "+str(data_makespan[resource][i])+" s and "+str(data_cost[resource][i])+" Euro\n"
paretoFront = open("serfri/results/pareto.txt", "w+")
paretoFront.write(text)
paretoFront.close()

# Plotting correspondant curves
import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(111)
for resource in columns[1:]:
    ax.plot(data_makespan[columns[0]], data_makespan[resource], label=resource)
    ax.set_xlabel("Number of instances")
    ax.set_ylabel("Makespan (s)")
    ax.legend()
plt.savefig("serfri/plots/makespan_curves.png")

fig = plt.figure()
ax = fig.add_subplot(111)
for resource in columns[1:]:
    ax.plot(data_cost[columns[0]], data_cost[resource], label=resource)
    ax.set_xlabel("Number of instances")
    ax.set_ylabel("Cost (euro)")
    plt.legend()
plt.savefig("serfri/plots/cost_curves.png")

# Normalizing matrices
min_makespans=[min([min(data_makespan[resource]) for resource in columns[1:]])]*len(columns[1:])
min_costs=[min([min(data_cost[resource]) for resource in columns[1:]])]*len(columns[1:])

for i in range(len(data_makespan)):
    for resource in columns[1:]:
        data_makespan[resource][i] = round((data_makespan[resource][i]-min_makespans[columns[1:].index(resource)])/min_makespans[columns[1:].index(resource)], 2)
        data_cost[resource][i] = round((data_cost[resource][i]-min_costs[columns[1:].index(resource)])/min_costs[columns[1:].index(resource)], 2)
data_makespan.to_csv("serfri/results/normalized_makespans.csv", index=False)

data_cost.to_csv("serfri/results/normalized_costs.csv", index=False)

# Maximizing the ratios
max_data = []
for i in range(len(data_cost)):
    line = [2**(i+1)]
    for resource in columns[1:]:
        line.append(max(data_cost[resource][i], data_makespan[resource][i]))
    max_data.append(line)
max_data = pd.DataFrame(data=max_data, columns=columns)
max_data.to_csv("serfri/results/maximized_ratios.csv", index=False)

# Finding the optimal pareto solution
min_=max_data[columns[1]][0]
for i in range(len(data_cost)):
    for resource in columns[1:]:
        if max_data[resource][i]<min_:
            min_ = max_data[resource][i]
            res = resource
            config = 2**(i+1)
solution = open("serfri/results/solution.txt", "w+")
solution.write("resource type : "+res+"\nnumber : "+str(config))
solution.close()
