import pandas as pd
import matplotlib.pyplot as plt
import statistics, sys
import numpy as np


project="test"
n = int(sys.argv[2])
nb_req = sys.argv[1]

sim_data=pd.read_csv("validation/results/sim_reachy_lille_"+nb_req+"_"+str(n)+".csv")
remote_data=pd.read_csv("realExp/results/reachy_lille_"+nb_req+"_"+str(n)+".csv")

#Data completion
# We choose data by the 2nd rank because in the real experimentation the first execution is biased by the loading of the program
remote_etl_duration = [remote_data["responseCN"][i]-remote_data["startClient"][i] for i in range(1, len(remote_data))]
sim_etl_duration = [sim_data["responseCN"][i]-sim_data["startClient"][i] for i in range(1, len(remote_data))]


#comp_var.plot(index_var, completion_var, label=str(n)+" nodes")

#Plotting ETL duration
plt.plot(range(2, int(nb_req)+1), remote_etl_duration, label="Real deployment")
plt.plot(range(2, int(nb_req)+1), sim_etl_duration, label="Simulated deployment")
plt.xlabel("Request order")
plt.ylabel("Duration of ETL process (s)")
plt.legend()
plt.savefig("validation/plots/etl_"+nb_req+"_"+str(n)+".png")