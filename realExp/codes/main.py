from fastapi import FastAPI
from time import time
import cv2, sys, os
import numpy as np
from typing import Union
from pydantic import BaseModel

class Image(BaseModel):
    content: Union[list, None] = None
    startClient: Union[float, None] = None
    ipClient: Union[str, None] = None
    order: Union[str, None] = None

app = FastAPI()

@app.post("/")
def create_item(req: Image):
    start = time()
    req_json = req.dict()
    srcimg = req_json["content"]
    startClient = req_json["startClient"]
    client = req_json["ipClient"]
    order = req_json["order"]
    image_name = client+order
    srcimg = np.array(srcimg)
    cv2.imwrite(image_name+".png", srcimg)
    size = os.path.getsize(image_name+".png")
    srcimg = cv2.imread(image_name+".png")
    os.remove(image_name+".png")
    if srcimg is None:
        res = None
        sys.exit(-1)
    cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    #cascade = cv2.CascadeClassifier("model.xml")
    #objects = cascade.detectMultiScale(srcimg, 1.1, 3)
    objects = cascade.detectMultiScale(srcimg, 1.2, 5, minSize=(30, 30))
    #objects, _ = hog.detectMultiScale(srcimg, winStride=(8,8))
    count = len(objects)
    if count == 0: res = False
    else: res = True
    end = time()

    return [{
        "isPerson": res,
        "startClient": startClient,
        "startComputing": start,
        "endComputing": end,
        "imageSize": size
    }]
