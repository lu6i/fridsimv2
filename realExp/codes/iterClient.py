import cv2, sys, requests, json, socket, threading
from multiprocessing.dummy import Pool
from multiprocessing import Process
from time import time

# Inputs
ips = [sys.argv[i] for i in range(1, len(sys.argv)-2)]
s= len(ips)
port = sys.argv[-2]
nbQueries = int(sys.argv[-1])
pool = Pool(100)

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def send(values):
    global futures
    cv2.startWindowThread()
    cap = cv2.VideoCapture(0)

    # Check if the webcam is opened correctly
    if not cap.isOpened():
        raise IOError("Cannot open webcam")

    
    i=0
    while i < nbQueries:
        
        try:
            _, frame = cap.read()
            #frame = cv2.resize(frame,(1280, 960), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
            values.append(pool.apply_async(requests.post, ["http://"+ips[i%s]+":"+port, json.dumps({"content": frame.tolist(), "startClient": time(), "ipClient": get_ip(), "order": str(i)}), {'Content-type': 'application/json', 'Accept': 'text/plain'}]))
            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        except:
            pass
        i+=1
    cap.release()
    futures=values

def get(values):
    global futures
    futures = values
    i=0
    while len(futures)!=0:
        """for future, i in zip(futures, range(len(futures))):
            print(2)
            json_res[str(i)] = {**future.get().json()[0], **{"responseCN": time(), "node": ips[i%s]}}
            #print(json_res[str(i)])"""
        
        json_res[str(i)] = {**futures[0].get().json()[0], **{"responseCN": time(), "node": ips[i%s]}}
        del futures[0]
        #print(json_res[str(i)])
        i+=1
        
json_res = {}
futures = []

if __name__ == '__main__':

    
    """send(futures)

    # futures is now a list of 10 futures.
    get(futures)
    #print(json_res)"""

    sending_thread = Process(target=send(futures))
    getting_thread = Process(target=get(futures))
    getting_thread.start()
    sending_thread.start()
    getting_thread.join()
    sending_thread.join()
    print(json_res)
    
    