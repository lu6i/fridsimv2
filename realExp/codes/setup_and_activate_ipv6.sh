#!/bin/bash

site=$1
node_name=$2
oar_job_id=$3

scp main.py root@$node_name:
scp requirements.txt root@$node_name:
ssh root@$node_name apt update
ssh root@$node_name apt install python3-pip -y
ssh root@$node_name apt install python3-opencv -y
ssh root@$node_name pip3 install -r requirements.txt
ssh root@$node_name dhclient -6 eno1
curl -i https://api.grid5000.fr/stable/sites/$site/firewall/$oar_job_id -d '[{"addr": "'$node_name'-ipv6.'$site'.grid5000.fr", "proto": "all"}]'

