import sys, json
from fridsim import remote_workflow

project = sys.argv[1]
print(project)
rst, mst, rct, mrt, rrt, rmt, robmt = remote_workflow("projects/"+project+"/robots_info", "projects/"+project+"/manager_info", "projects/"+project+"/fcns_info")
a_traces = {}
r_traces = {}
um_traces = {}
m_traces = {}
g_traces = {}
robm_traces={}
#requests ids
rids = list(rst.keys())
for rid in rids:
    a_traces[rid]={"SentBy": rst[rid]["robotName"], "SharedBy": mst[rid]["managerName"], "ProcessedBy": rct[rid]["resourceName"], "ResourcePricePerHour": rct[rid]["costPerHour"], "ResourceType": rct[rid]["resourceType"], "Size": rst[rid]["requestSize"], "SentAt": rst[rid]["startRequest"], "SharedAt": mst[rid]["startSharing"], "ProcessedAt": rct[rid]["startComputing"], "EndedAt": rct[rid]["endComputing"], "RespondedAt": mrt[rid]["startResponding"], "ReceivedAt": rrt[rid]["receivingRequest"]}
    r_traces[rid]={"RuntimeAtSending": rst[rid]["runTime"], "RuntimeAtSharing": mst[rid]["runTime"], "RuntimeAtProcessing": rct[rid]["runTime"], "RuntimeAtResponding": mrt[rid]["runTime"], "RuntimeAtReceiving": rrt[rid]["runTime"]}
    um_traces[rid]={"UsedMemoryAtSending": rst[rid]["usedRAM"], "UsedMemoryAtSharing": mst[rid]["usedRAM"], "UsedMemoryAtProcessing": rct[rid]["usedRAM"], "UsedMemoryAtResponding": mrt[rid]["usedRAM"], "UsedMemoryAtReceiving": rrt[rid]["usedRAM"]}
m_traces = rmt
robm_traces = robmt

primary_traces_types = [rst, mst, rct, mrt, rrt, rmt, robmt]
for primary_traces_type in primary_traces_types:
    del primary_traces_type
del primary_traces_types

X = list(a_traces.keys())
resources_names = []
startings = []
endings = []
costsPH = []
for k in X:
    resource_name = a_traces[k]["ProcessedBy"]
    if not (resource_name in resources_names):
        startings.append(float(a_traces[k]["ProcessedAt"]))
        endings.append(0)
        resources_names.append(resource_name)
        costsPH.append(float(a_traces[k]["ResourcePricePerHour"]))
    else:
        ind = resources_names.index(resource_name)
        endings[ind]=float(a_traces[k]["EndedAt"])
makespans=[e-s for e, s in zip(endings, startings)]
costs=[m*c for m, c in zip(makespans, costsPH)]

for resource_name, makespan, cost, starting, ending, costph in zip(resources_names, makespans, costs, startings, endings, costsPH):
    g_traces[resource_name]={"RessourceName": resource_name, "Starting":starting, "Ending":ending, "Makespan": makespan, "CostPerHour": costph, "UsageCost": cost}
g_traces["Total"]={"RessourceName": "All", "Starting":min(startings), "Ending":max(endings), "Makespan": max(endings)-min(startings), "CostPerHour": "Not Defined Property", "UsageCost": sum(costs)}

del X
del resources_names
del startings
del endings
del costsPH

metrics = ["accuracy", "runtime", "used_memory", "resources_moving", "robot_moving", "global"]
traces_types= [a_traces, r_traces, um_traces, m_traces, robm_traces, g_traces]

for traces_type, metric in zip(traces_types, metrics):
    json.dump(traces_type, open("projects/"+project+"/simulation_results/"+metric+"_traces.json", "w"), indent=4)
    del traces_type
del traces_types