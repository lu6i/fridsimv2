import sys, os

wrong_json_file_name = sys.argv[1]
wrong_json_file = open(wrong_json_file_name, "r+")
content = list(wrong_json_file)[0]
content = content.replace("'", "\"")
content = content.replace("True", "true")
content = content.replace("False", "false")
wrong_json_file.close()
correct_json_file = open(wrong_json_file_name, "w+")
correct_json_file.write(content)
correct_json_file.close()

os.system("python3 json2csv.py "+wrong_json_file_name)
