import statistics
import numpy as np
from time import time
from services import *
from random import uniform
import sys, json, tracemalloc
from simgrid import this_actor, Mailbox, Comm, NetZone, LinkInRoute, Engine, Actor, Host, Link

def memory():
    with open('/proc/meminfo', 'r') as mem:
        ret = {}
        tmp = 0
        for i in mem:
            sline = i.split()
            if str(sline[0]) == 'MemTotal:':
                ret['total'] = int(sline[1])
            elif str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
                tmp += int(sline[1])
    return ret['total']

def local_platform(robots_info, ecns_info):

    robots = list(robots_info.keys())
    ecns = list(ecns_info.keys())

    root_zone = NetZone.create_floyd_zone("fog_robotics_platform")

    robotHosts=[]

    for rp in robots:
        robot=root_zone.create_host(rp, robots_info[rp]["speed"])
        #link = root_zone.create_link(rp+"-"+rp, "20MBps").set_latency("0s")
        #root_zone.add_route(robot.netpoint, robot.netpoint, None, None, [LinkInRoute(link)], True)
        robotHosts.append(robot)
    
    for ecn in ecns:
        for j in range(int(ecns_info[ecn]["nb_cores"])*int(ecns_info[ecn]["nb_instances"])):
            core = root_zone.create_host(ecn+str(j), ecns_info[ecn]["speed"])
            for lp in ecns_info[ecn]["serving"]:
                link = root_zone.create_link(lp["robot"]+"-"+ecn+str(j), lp["bw"])
                link.set_latency(lp["lat"])
                root_zone.add_route(robotHosts[robots.index(lp["robot"])].netpoint, core.netpoint, None, None, [LinkInRoute(link)], True)

def remote_platform(robots_info, manager_info, fcns_info):

    robots = list(robots_info.keys())
    fcns = list(fcns_info.keys())

    root_zone = NetZone.create_floyd_zone("fog_robotics_platform")
    fog_zone = NetZone.create_floyd_zone("fog")

    fog_zone.set_parent(root_zone)

    manager = fog_zone.create_host(manager_info["name"], manager_info["speed"])
    resourcesMobilityManager = fog_zone.create_host("ResourcesMobilityManager", "50Gf")
    robotsMobilityManager = fog_zone.create_host("RobotsMobilityManager", "50Gf")

    for robot in robots:
        edge_zone = NetZone.create_full_zone("edge_"+robot)
        edge_zone.set_parent(root_zone)
        robotHost = edge_zone.create_host(robot, robots_info[robot]["speed"])
        link = root_zone.create_link(robot+"-"+manager_info["name"], robots_info[robot]["bw"])
        link.set_latency(robots_info[robot]["lat"])
        root_zone.add_route(edge_zone.netpoint, fog_zone.netpoint, robotHost.netpoint, manager.netpoint, [LinkInRoute(link)], True)
    
    for fcn in fcns:
        for j in range(int(fcns_info[fcn]["nb_cores"])*int(fcns_info[fcn]["nb_instances"])):
            core = fog_zone.create_host(fcn+str(j), fcns_info[fcn]["speed"])
            link = fog_zone.create_link(manager_info["name"]+"-"+fcn+str(j), fcns_info[fcn]["bw"])
            link.set_latency(fcns_info[fcn]["lat"])
            fog_zone.add_route(manager.netpoint, core.netpoint, None, None, [LinkInRoute(link)], True)

class Traces:
    nbq=0
    rs={} # robot sending traces
    ms={} # manager sharing traces
    rc={} # resource computing traces
    mr={} # manager responding traces
    rr={} # robot receiving traces
    rm={} # resource mobility traces
    robm={} # robot mobility traces
    
class Sending:
    def __init__(self, *args):
        self.args = args

    def __call__(self):

        # Here a specific robot sends requests to the manager
        nb_queries, frequency, manager, service= int(self.args[0]), float(self.args[1]), self.args[2], self.args[3]
        comms = []
        mb = Mailbox.by_name(manager)
        for j in range(nb_queries):
            #workload = randint(min_workload, max_workload)
            startIssue = Engine.clock
            func = globals()[service]
            ptime, dataSize = func(str(j), startIssue)
            message_j = str(ptime)+"|"+this_actor.get_host().name+"|"+str(Traces.nbq)
            comm = mb.put_async(message_j, len(message_j))
            #this_actor.info(str(Traces.nbq))
            comms.append(comm)
            l=len(list(Traces.rs.keys()))
            Traces.rs[str(Traces.nbq)] = {"robotName":this_actor.get_host().name, "requestSize": dataSize, "startRequest": startIssue, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
            Traces.nbq+=1
            this_actor.sleep_for(1/frequency)
        Comm.wait_all(comms)

class Managing:

    def __init__(self, *args):
        self.args = args

    def __call__(self):
        
        # Here the manager receives requests from robots and sends them to processing cores
        nb_queries, cores = int(self.args[0]), self.args[1].split(" ")
        p_comms = []
        comms = []
        manager_name = this_actor.get_host().name
        mb  = Mailbox.by_name(manager_name)
        j=0
        while j<nb_queries:
            p_comms.append(mb.get_async())
            j+=1
        j=0
        while j<nb_queries:
            index = Comm.wait_any([comm for (comm, _) in p_comms])
            _, async_data = p_comms[index]
            msg=async_data.get()
            core_j = cores[j%len(cores)]
            mb_core = Mailbox.by_name(core_j)
            key1=len(Traces.ms)
            key2=int(int(msg.split("|")[2])/2)
            key = str(key1) if key2<=key1 else str(key2)
            wl=msg.split("|")[0]
            hn=msg.split("|")[1]
            msg=wl+"|"+hn+"|"+key
            comm = mb_core.put_async(msg, len(msg))
            #this_actor.info(key)
            comms.append(comm)
            j+=1
            p_comms.pop(index)
            startSharing = Engine.clock
            l=len(list(Traces.ms.keys()))
            Traces.ms[key] = {"managerName":this_actor.get_host().name, "startSharing": startSharing, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
        Comm.wait_all(comms)

class Computing:

    def __init__(self, *args):
        self.args = args

    def __call__(self):
        
        # Here each processing receives specific requests, processes on them and sends JSON response to the manager
        nb_queries, nb_cores, manager_name, i, nodeType, nodeName, cost = int(self.args[0]), int(self.args[1]), self.args[2], int(self.args[3]), self.args[4], self.args[5], self.args[6]

        q = int(nb_queries/nb_cores)
        r = nb_queries%nb_cores
        nb_queries_core_i = q+1 if i<r else q

        core_i = this_actor.get_host().name
        mb_core = Mailbox.by_name(core_i)
        q = int(nb_queries/nb_cores)
        r = nb_queries%nb_cores
        nb_queries_core_i = q if i>=r else q+1
        p_comms = []
        comms = []
        j=0
        while j<nb_queries_core_i:
            p_comms.append(mb_core.get_async())
            j+=1
        j=0
        while j<nb_queries_core_i:
            index = Comm.wait_any([comm for (comm, _) in p_comms])
            _, async_data = p_comms[index]
            msg=async_data.get()
            amount_flops = float(msg.split("|")[0])
            #this_actor.info(msg.split("|")[2])
            startComputing = Engine.clock  
            this_actor.execute(amount_flops*this_actor.get_host().speed)
            #this_actor.info("End Computing")
            endComputing = Engine.clock
            #this_actor.info(msg.split("|")[2]+" "+str(startComputing)+" "+str(endComputing))
            mb_manager = Mailbox.by_name(manager_name)
            mb_manager.put(msg, len(msg))
            l=len(list(Traces.rc.keys()))
            Traces.rc[msg.split("|")[2]] = {"resourceName":nodeName, "costPerHour":cost, "resourceType":nodeType, "startComputing": startComputing, "endComputing": endComputing, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
            
            j+=1
            p_comms.pop(index)

class HFogComputing:

    def __init__(self, *args):
        self.args = args

    def __call__(self):
        
        # Here each processing receives specific requests, processes on them and sends JSON response to the manager
        nb_queries, nb_cores, manager_name, i, nodeType, nodeName, cost = int(self.args[0]), int(self.args[1]), self.args[2], int(self.args[3]), self.args[4], self.args[5], self.args[6]

        q = int(nb_queries/nb_cores)
        r = nb_queries%nb_cores
        nb_queries_core_i = q+1 if i<r else q

        core_i = this_actor.get_host().name
        mb_core = Mailbox.by_name(core_i)
        q = int(nb_queries/nb_cores)
        r = nb_queries%nb_cores
        nb_queries_core_i = q if i>=r else q+1
        p_comms = []
        comms = []
        j=0
        while j<nb_queries_core_i:
            p_comms.append(mb_core.get_async())
            j+=1
        j=0
        while j<nb_queries_core_i:
            index = Comm.wait_any([comm for (comm, _) in p_comms])
            _, async_data = p_comms[index]
            msg=async_data.get()
            amount_flops = float(msg.split("|")[0])
            #this_actor.info(msg.split("|")[2])
            startComputing = Engine.clock  
            activity = this_actor.exec_init(amount_flops*this_actor.get_host().speed).start()
            while not activity.test():
                this_actor.sleep_for(1)
            activity.wait()
            #this_actor.info("End Computing")
            endComputing = Engine.clock
            #this_actor.info(msg.split("|")[2]+" "+str(startComputing)+" "+str(endComputing))
            mb_manager = Mailbox.by_name(manager_name)
            mb_manager.put(msg, len(msg))
            l=len(list(Traces.rc.keys()))
            Traces.rc[msg.split("|")[2]] = {"resourceName":nodeName, "costPerHour":cost, "resourceType":nodeType, "startComputing": startComputing, "endComputing": endComputing, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
            
            j+=1
            p_comms.pop(index)


def remote_workflow_HCF(robots_info_file, manager_info_file, fcns_info_file):
    response_size=62
    robots_info = json.load(open(robots_info_file+".json"))
    manager_info = json.load(open(manager_info_file+".json"))
    fcns_info = json.load(open(fcns_info_file+".json"))
    e = Engine(sys.argv)
    remote_platform(robots_info, manager_info, fcns_info)
    robots = list(robots_info.keys())
    fcns = list(fcns_info.keys())
    for robot in robots:
        for requests in robots_info[robot]["queries"]:
            # Trying to find the best number of clusters
            v = int(len(fcns)/2)
            means = []
            for m in range(2, v if v>2 else 3):
                clusters = []
                lats = []
                instances = []
                types = []
                orders = []
                for fcn in fcns:
                    for instance in range(int(fcns_info[fcn]["nb_instances"])):
                        inst = fcn+str(instance*int(fcns_info[fcn]["nb_cores"]))
                        instances.append(inst)
                        link = Link.by_name(manager_info["name"]+"-"+inst)
                        lats.append(link.latency)
                        orders.append(instance)
                        types.append(fcn)
                l, L = min(lats), max(lats)
                for instance in instances:
                    lat = lats[instances.index(instance)]
                    order = int((lat-l)/(L-l))
                    clusters.append(order+1 if lat<L else order)
                stds=[]
                for cluster in range(min(clusters), max(clusters)+1):
                    p_lats = []
                    for i in range(len(clusters)):
                        if clusters[i]==cluster:
                            p_lats.append(lats[i])
                    stds.append(np.std(p_lats))
                means.append(statistics.mean(stds))
            ind_m = means.index(min(means))
            m = ind_m+2

            # Building clusters with the optimal number of clusters
            clusters = []
            lats = []
            instances = []
            types = []
            orders = []
            for fcn in fcns:
                for instance in range(int(fcns_info[fcn]["nb_instances"])):
                    inst = fcn+str(instance*int(fcns_info[fcn]["nb_cores"]))
                    instances.append(inst)
                    link = Link.by_name(manager_info["name"]+"-"+inst)
                    lats.append(link.latency)
                    orders.append(instance)
                    types.append(fcn)
            l, L = min(lats), max(lats)
            for instance in instances:
                lat = lats[instances.index(instance)]
                order = int((lat-l)/(L-l))
                clusters.append(order+1 if lat<L else order)    

            used_fcns_info = {}
            service = requests["service"]
            cores = ""
            cid=0
            cs=[]
            init_insts=[]
            init_types=[]
            init_costs=[]
            for fcn in fcns:
                if service in fcns_info[fcn]["services"] and clusters[fcns.index(fcn)]==1:
                    used_fcns_info[fcn]=fcns_info[fcn]
                    used_instances = []
                    for instance in instances:
                        if clusters[instances.index(instance)]==1 and types[instances.index(instance)]==fcn: used_instances.append(instance)
                    c1=[]
                    inter_insts1=[]
                    inter_types1=[]
                    inter_costs1=[]
                    for j in range(len(used_instances)):
                        c=[]
                        inter_insts=[]
                        inter_types=[]
                        inter_costs=[]
                        cid = int(used_instances[j].split(fcn)[1])
                        
                        for i in range(int(used_fcns_info[fcn]["nb_cores"])):
                            c.append(fcn+str(cid))
                            inter_insts.append(used_instances[j])
                            inter_types.append(fcn)
                            inter_costs.append(used_fcns_info[fcn]["cost"])
                            cid+=1
                        c1.append(c)
                        inter_insts1.append(inter_insts)
                        inter_types1.append(inter_types)
                        inter_costs1.append(inter_costs)
                    cs.append(c1)
                    init_insts.append(inter_insts1)
                    init_types.append(inter_types1)
                    init_costs.append(inter_costs1)

            for i in range(len(cs)):
                for j in range(len(cs[i])):
                    for k in range(len(cs[i][j])):
                        cores+=" "+cs[i][j][k]
            
            cores=t3D(cs)
            instances = t3D(init_insts)
            types = t3D(init_types)
            costs = t3D(init_costs)
            
            nb_cores = len(cores.split(" "))
            
            Actor.create("Sending", Host.by_name(robot), Sending(requests["nb_queries"], robots_info[robot]["frequency"], manager_info["name"], robots_info[robot]["min_queries_size"], robots_info[robot]["max_queries_size"]))

            Actor.create("Managing", Host.by_name(manager_info["name"]), Managing(requests["nb_queries"], cores))
           
            for core, instance, type, cost in zip(cores.split(" "), instances.split(" "), types.split(" "), costs.split(" ")):
                Actor.create("Computing", Host.by_name(core), HFogComputing(requests["nb_queries"], str(nb_cores), manager_info["name"], str(cores.split(" ").index(core)), "Fog", instance, cost))

            Actor.create("Responding", Host.by_name(manager_info["name"]), Responding(robot, requests["nb_queries"], str(response_size)))

            Actor.create("Receiving", Host.by_name(robot), Receiving(requests["nb_queries"]))
    for fcn in fcns:
        Actor.create("ResourceMoving", Host.by_name("ResourcesMobilityManager"), ResourceMoving(manager_info["name"], fcns_info[fcn]))
    for robot in robots:
        Actor.create("RobotMoving", Host.by_name("RobotsMobilityManager"), RobotMoving(manager_info["name"], robots[robot]))

    tracemalloc.start()
    e.run()
    tracemalloc.stop()
    return Traces.rs, Traces.ms, Traces.rc, Traces.mr, Traces.rr, Traces.rm, Traces.robm

class ResourceMoving:

    def __init__(self, *args):
        self.args = args

    def __call__(self):
        manager = self.args[0]
        fcns_info = self.args[1]
        factors = {"GBps":2**30, "MBps":2**20, "KBps":2**10, "Bps":1}
        if fcns_info["mobility"]["dynamic"]:
            
            if "random_mobility" in list(fcns_info["mobility"].keys()) != 0:
                lat_unit = fcns_info["mobility"]["random_mobility"]["min_lat"][-2:]
                bw_unit = fcns_info["mobility"]["random_mobility"]["min_bw"][-4:]
                factor = factors[bw_unit]
                min_lat = float(fcns_info["mobility"]["random_mobility"]["min_lat"][:-2])
                max_lat = float(fcns_info["mobility"]["random_mobility"]["max_lat"][:-2])
                min_bw = float(fcns_info["mobility"]["random_mobility"]["min_bw"][:-4])
                max_bw = float(fcns_info["mobility"]["random_mobility"]["max_bw"][:-4])
                this_actor.sleep_for(float(fcns_info["mobility"]["random_mobility"]["period"]))
                i=0
                while i<int(fcns_info["mobility"]["random_mobility"]["nb_motions"]):
                    
                    for j in range(int(fcns_info["nb_instances"])):
                        new_lat = uniform(min_lat, max_lat)
                        new_bw = uniform(min_bw, max_bw)
                        for i in range(int(fcns_info["nb_cores"])):
                            ind = j*int(fcns_info["nb_cores"])+i
                            #this_actor.set_host(Host.by_name(fcns_info["name"]+str(ind)))
                            link = Link.by_name(manager+"-"+fcns_info["name"]+str(ind))
                            oldBw = str(link.bandwidth/2**20)+"MBps"
                            oldLat = str(link.latency)+"s"
                            link.set_latency(str(new_lat)+lat_unit)
                            link.set_bandwidth(new_bw*factor)
                            #this_actor.info(fcns_info["name"]+str(ind)+" Moving")
                            startMoving = Engine.clock
                            l=len(list(Traces.rm.keys()))
                            Traces.rm[str(l)] = {"resourceName":fcns_info["name"]+str(ind), "startMoving":startMoving, "oldBw":oldBw, "oldLat": oldLat, "newBw": str(new_bw)+bw_unit, "newLat": str(new_lat)+lat_unit, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
                    this_actor.sleep_for(float(fcns_info["mobility"]["random_mobility"]["period"]))
                    i+=1
            else:
                mob_infos = fcns_info["mobility"]["defined_mobility"]
                for mob_info in mob_infos:
                    new_lat = float(mob_info["lat"][:-2])
                    new_bw = float(mob_info["bw"][:-4])
                    period = float(mob_info["after"])
                    lat_unit = mob_info["lat"][-2:]
                    bw_unit = mob_info["bw"][-4:]
                    factor = factors[bw_unit]
                    for j in range(int(fcns_info["nb_instances"])*int(fcns_info["nb_cores"])):
                        #this_actor.set_host(Host.by_name(fcns_info["name"]+str(j)))
                        link = Link.by_name(manager+"-"+fcns_info["name"]+str(j))
                        oldBw = str(link.bandwidth/2**20)+"MBps"
                        oldLat = str(link.latency)+"s"
                        link.set_latency(str(new_lat)+lat_unit)
                        link.set_bandwidth(new_bw*factor)
                        #this_actor.info(fcns_info["name"]+str(j)+" Moving")
                        startMoving = Engine.clock
                        l=len(list(Traces.rm.keys()))
                        Traces.rm[str(l)] = {"resourceName":fcns_info["name"]+str(j), "startMoving":startMoving, "oldBw":oldBw, "oldLat": oldLat, "newBw": str(new_bw)+bw_unit, "newLat": str(new_lat)+lat_unit, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
                    this_actor.sleep_for(period)

class RobotMoving:

    def __init__(self, *args):
        self.args = args

    def __call__(self):
        manager = self.args[0]
        robot = self.args[1]
        factors = {"GBps":2**30, "MBps":2**20, "KBps":2**10, "Bps":1}
        if robot["mobility"]["dynamic"]:
            
            if "random_mobility" in list(robot["mobility"].keys()) != 0:
                lat_unit = robot["mobility"]["random_mobility"]["min_lat"][-2:]
                bw_unit = robot["mobility"]["random_mobility"]["min_bw"][-4:]
                factor = factors[bw_unit]
                min_lat = float(robot["mobility"]["random_mobility"]["min_lat"][:-2])
                max_lat = float(robot["mobility"]["random_mobility"]["max_lat"][:-2])
                min_bw = float(robot["mobility"]["random_mobility"]["min_bw"][:-4])
                max_bw = float(robot["mobility"]["random_mobility"]["max_bw"][:-4])
                this_actor.sleep_for(float(robot["mobility"]["random_mobility"]["period"]))
                i=0
                while i<int(robot["mobility"]["random_mobility"]["nb_motions"]):
                    new_lat = uniform(min_lat, max_lat)
                    new_bw = uniform(min_bw, max_bw)
                    link = Link.by_name(robot["name"]+"-"+manager)
                    oldBw = str(link.bandwidth/2**20)+"MBps"
                    oldLat = str(link.latency)+"s"
                    link.set_latency(str(new_lat)+lat_unit)
                    link.set_bandwidth(new_bw*factor)
                    #this_actor.info(robot["name"]+" Moving")
                    startMoving = Engine.clock
                    l=len(list(Traces.rm.keys()))
                    Traces.robm[str(l)] = {"resourceName":robot["name"], "startMoving":startMoving, "oldBw":oldBw, "oldLat": oldLat, "newBw": str(new_bw)+bw_unit, "newLat": str(new_lat)+lat_unit, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
                    this_actor.sleep_for(float(robot["mobility"]["random_mobility"]["period"]))
                    i+=1
            else:
                mob_infos = robot["mobility"]["defined_mobility"]
                for mob_info in mob_infos:
                    new_lat = float(mob_info["lat"][:-2])
                    new_bw = float(mob_info["bw"][:-4])
                    period = float(mob_info["after"])
                    lat_unit = mob_info["lat"][-2:]
                    bw_unit = mob_info["bw"][-4:]
                    factor = factors[bw_unit]
                    link = Link.by_name(robot["name"]+"-"+manager)
                    oldBw = str(link.bandwidth/2**20)+"MBps"
                    oldLat = str(link.latency)+"s"
                    link.set_latency(str(new_lat)+lat_unit)
                    link.set_bandwidth(new_bw*factor)
                    #this_actor.info(robot["name"]+" Moving")
                    startMoving = Engine.clock
                    l=len(list(Traces.rm.keys()))
                    Traces.robm[str(l)] = {"resourceName":robot["name"], "startMoving":startMoving, "oldBw":oldBw, "oldLat": oldLat, "newBw": str(new_bw)+bw_unit, "newLat": str(new_lat)+lat_unit, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
                    this_actor.sleep_for(period)

class Responding:

    def __init__(self, *args):
        self.args = args

    def __call__(self):

        # Here the manager receives JSON response from processing cores and sends them to corresponding robot
        robot_name, nb_queries, response_size = self.args[0], int(self.args[1]), self.args[2]
        p_comms = []
        comms = []
        mb_manager  = Mailbox.by_name(this_actor.get_host().name)
        j=0
        while j<nb_queries:
            startResponding = Engine.clock
            msg = mb_manager.get()
            mb_robot = Mailbox.by_name(robot_name)
            key1=len(Traces.mr)
            key2=int((int(msg.split("|")[2])-1)/2)
            key = str(key1) if key2!=key1 else str(key2)
            wl=msg.split("|")[0]
            hn=msg.split("|")[1]
            msg=wl+"|"+hn+"|"+key
            mb_robot.put(msg, len(msg))
            #comms.append(comm)
            j+=1
            #p_comms.pop(index)
            #this_actor.info(str(startResponding))
            l=len(list(Traces.mr.keys()))
            #this_actor.info(str(l))
            Traces.mr[str(l)] = {"managerName":this_actor.get_host().name, "startResponding": Engine.clock, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
        #Comm.wait_all(comms)

class Receiving:

    def __init__(self, *args):
        self.args = args

    def __call__(self):

        # Here each processing receives specific requests, processes on them and sends JSON response to the manager
        nb_queries= int(self.args[0])

        # Here the robots receive JSON responses from the manager
        robot_i = this_actor.get_host().name
        p_comms = []
        mb_robot_i  = Mailbox.by_name(robot_i)
        j=0
        while j<nb_queries:
            msg=mb_robot_i.get()
            key1=len(Traces.rr)
            key2=int((int(msg.split("|")[2])-1)/2)
            key = str(key1) if key2!=key1 else str(key2)
            wl=msg.split("|")[0]
            hn=msg.split("|")[1]
            msg=wl+"|"+hn+"|"+key
            #this_actor.info(key)
            receivingRequest = Engine.clock
            l=len(list(Traces.rr.keys()))
            Traces.rr[key] = {"robotName":this_actor.get_host().name, "receivingRequest": receivingRequest, "runTime": time(), "usedRAM": tracemalloc.get_traced_memory()[0]*100/memory()}
            j+=1
            #p_comms.pop(index)

def local_workflow(robots_info_file, ecns_info_file):
    robots_info = json.load(open(robots_info_file+".json"))
    ecns_info = json.load(open(ecns_info_file+".json"))
    e = Engine(sys.argv)
    local_platform(robots_info, ecns_info)
    robots = list(robots_info.keys())
    ecns = list(ecns_info.keys())
    for robot in robots:
        for requests in robots_info[robot]["queries"]:
            Actor.create("Sending", Host.by_name(robot), Sending(requests["nb_queries"], requests["frequency"], robot, robots_info[robot]["min_queries_size"], robots_info[robot]["max_queries_size"]))
            used_ecns_info = {}
            service = requests["service"]
            cores = ""
            cid=0
            cs=[]
            for ecn in ecns:
                if service in ecns_info[ecn]["services"]:
                    used_ecns_info[ecn]=ecns_info[ecn]
                    c1=[]
                    for j in range(int(ecns_info[ecn]["nb_instances"])):
                        c=[]
                        for i in range(int(ecns_info[ecn]["nb_cores"])):
                            c.append(ecn+str(cid))
                            cid+=1
                        c1.append(c)
                    cs.append(c1)
                cid=0

            for i in range(len(cs)):
                for j in range(len(cs[i])):
                    for k in range(len(cs[i][j])):
                        cores+=" "+cs[i][j][k]
            
            cores=t3D(cs)
            
            nb_cores = len(cores.split(" "))
            used_ecns = list(used_ecns_info.keys())
            cores = cores.strip()

            Actor.create("Managing", Host.by_name(robot), Managing(requests["nb_queries"], cores))
            k=0
            for used_ecn in used_ecns:
                for j in range(int(used_ecns_info[used_ecn]["nb_cores"])*int(used_ecns_info[used_ecn]["nb_instances"])):
                    id_ = int((j-1)/int(used_ecns_info[used_ecn]["nb_cores"]))+1
                    Actor.create("Computing", Host.by_name(used_ecn+str(j)), Computing(requests["nb_queries"], str(nb_cores), robot, str(cores.split(" ").index(used_ecn+str(j))), "Fog", used_ecn+str(id_), used_ecns_info[used_ecn]["cost"]))
                    k+=1

            Actor.create("Receiving", Host.by_name(robot), Receiving(requests["nb_queries"]))

    tracemalloc.start()
    e.run()
    tracemalloc.stop()
    return Traces.rs, Traces.ms, Traces.rc, Traces.mr, Traces.rr, Traces.rm

def hybrid_workflow(robots_info, manager_info, ecns_info, fcns_info, response_size):
    pass

def t3D(table):
    m=0
    n=0
    for t1 in table:
        if len(t1)>=m:
            m=len(t1)
        for t2 in t1:
            if len(t2)>=n:
                n=len(t2)
    res=""
    for j in range(n):
        for i in range(m):
            for t1 in table:
                if i<len(t1):
                    if j<len(t1[i]):
                        res+=" "+t1[i][j]
    return res.strip()

def remote_workflow(robots_info_file, manager_info_file, fcns_info_file):
    response_size=62
    robots_info = json.load(open(robots_info_file+".json"))
    manager_info = json.load(open(manager_info_file+".json"))
    fcns_info = json.load(open(fcns_info_file+".json"))
    e = Engine(sys.argv)
    remote_platform(robots_info, manager_info, fcns_info)
    robots = list(robots_info.keys())
    fcns = list(fcns_info.keys())
    for robot in robots:
        for requests in robots_info[robot]["queries"]:
            service = requests["service"]
            Actor.create("Sending", Host.by_name(robot), Sending(requests["nb_queries"], requests["frequency"], manager_info["name"], service))
            used_fcns_info = {}
            cores = ""
            cid=0
            cs=[]
            for fcn in fcns:
                if service in fcns_info[fcn]["services"]:
                    used_fcns_info[fcn]=fcns_info[fcn]
                    c1=[]
                    for j in range(int(fcns_info[fcn]["nb_instances"])):
                        c=[]
                        for i in range(int(fcns_info[fcn]["nb_cores"])):
                            c.append(fcn+str(cid))
                            cid+=1
                        c1.append(c)
                    cs.append(c1)
                cid=0

            for i in range(len(cs)):
                for j in range(len(cs[i])):
                    for k in range(len(cs[i][j])):
                        cores+=" "+cs[i][j][k]
            
            cores=t3D(cs)
            
            nb_cores = len(cores.split(" "))
            used_fcns = list(used_fcns_info.keys())
            cores = cores.strip()

            Actor.create("Managing", Host.by_name(manager_info["name"]), Managing(requests["nb_queries"], cores))
            k=0
            for used_fcn in used_fcns:
                for j in range(int(used_fcns_info[used_fcn]["nb_cores"])*int(used_fcns_info[used_fcn]["nb_instances"])):
                    id_ = int((j-1)/int(used_fcns_info[used_fcn]["nb_cores"]))+1
                    Actor.create("Computing", Host.by_name(used_fcn+str(j)), Computing(requests["nb_queries"], str(nb_cores), manager_info["name"], str(cores.split(" ").index(used_fcn+str(j))), "Fog", used_fcn+str(id_), used_fcns_info[used_fcn]["cost"]))
                    k+=1
            
            Actor.create("Responding", Host.by_name(manager_info["name"]), Responding(robot, requests["nb_queries"], str(response_size)))

            Actor.create("Receiving", Host.by_name(robot), Receiving(requests["nb_queries"]))
    for fcn in fcns:
        Actor.create("ResourceMoving", Host.by_name("ResourcesMobilityManager"), ResourceMoving(manager_info["name"], fcns_info[fcn]))
    for robot in robots:
        Actor.create("RobotMoving", Host.by_name("RobotsMobilityManager"), RobotMoving(manager_info["name"], robots_info[robot]))

    tracemalloc.start()
    e.run()
    tracemalloc.stop()
    return Traces.rs, Traces.ms, Traces.rc, Traces.mr, Traces.rr, Traces.rm, Traces.robm



# if __name__=="__main__":
#     project = "r2_30_5"
#     remote_workflow("projects/"+project+"/robots_info", "projects/"+project+"/manager_info", "projects/"+project+"/fcns_info")
