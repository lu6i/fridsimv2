import json, sys
import pandas as pd

json_file = sys.argv[1]

with open(json_file) as f:
    results = json.load(f)

res_keys = list(results.keys())
commun_keys = list(results["0"].keys())

dataArray = []

for key in res_keys:
    dataArray.append([results[key][k] for k in commun_keys])

data = pd.DataFrame(data=dataArray, columns=commun_keys)
data = data.sort_values(by="startComputing").reset_index(drop=True)
data.to_csv(json_file.split(".")[0]+".csv", index=False)